from nornir import InitNornir
from nornir_scrapli.tasks import send_configs
from nornir_utils.plugins.functions import print_result
from nornir_jinja2.plugins.tasks import template_file
from nornir_utils.plugins.tasks.data import load_yaml

nr = InitNornir()

# Create a function that load a dynamic variable file
def load_vars(task):
    data = task.run(task=load_yaml, file=f"./host_vars/{task.host}.yaml")
    
    # bind the results of the file to a new dictionary key in the host variable file
    task.host["facts"] = data.result
    
    # load the main function
    test_template(task=task)


def test_template(task):
    template = task.run(task= template_file, template=f"ospf/{task.host.platform}-ospf.j2", path="templates")
    task.host["ospf_config"] = template.result
    rendered = task.host["ospf_config"]
    configuration = rendered.splitlines()
    task.run(task=send_configs, configs=configuration)

# Run the load_vars function, the load_vars function will in his turn; load the templating function
result = nr.run(task=load_vars)
print_result(result=result)

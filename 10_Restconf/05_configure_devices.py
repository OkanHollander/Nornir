import requests
from nornir import InitNornir
from nornir_utils.plugins.functions import print_result
from nornir.core.task import Result
from nornir_utils.plugins.tasks.data import load_yaml

nr = InitNornir()
requests.packages.urllib3.disable_warnings()

headers = {"Accept": "application/yang-data+json", "Content-Type": "application/yang-data+json"}
chained_url = "data/openconfig-acl:acl"

def load_data(task):
    data = task.run(task=load_yaml, file=f"./host_vars/{task.host}_acl.yml")
    task.host["acl_facts"] = data.result



def restconf_test(task, chained_url, headers):
    url = f"https://{task.host.hostname}:443/restconf/"
    response = requests.put(url=url + chained_url,
                            headers=headers,
                            auth=(f"{task.host.username}", f"{task.host.password}"),
                            verify=False,
                            json=task.host["acl_facts"]["configure_acl"])
    return Result(host=task.host, result=response)

load_result = nr.run(task=load_data)
print_result(result=load_result)
configure_results = nr.run(task=restconf_test, 
                           chained_url=chained_url,
                           headers=headers)
print_result(result=configure_results)
import requests
from nornir import InitNornir
from nornir_utils.plugins.functions import print_result
from nornir_utils.plugins.tasks.data import load_yaml
from nornir.core.task import Result
import ipdb

nr = InitNornir()
requests.packages.urllib3.disable_warnings()

headers = {"Accept": "application/yang-data+json"}

def load_vars(task):
    data = task.run(task=load_yaml, file=f"./host_vars/{task.host}.yaml")
    
    # bind the results of the file to a new dictionary key in the host variable file
    task.host["bgp_facts"] = data.result
    
    # load the main function
    restconf_test(task=task)

def restconf_test(task):
    url = f"https://{task.host.hostname}:443/restconf/data/native/router/Cisco-IOS-XE-bgp:bgp={task.host['bgp_facts']['bgp']['asn']}/neighbor=172.17.1.13"
    response = requests.get(url=url,
                            headers=headers,
                            auth=(f"{task.host.username}", f"{task.host.password}"),
                            verify=False)
    task.host["facts"] = response.json()
    remote_asn = task.host["facts"]["Cisco-IOS-XE-bgp:neighbor"]["remote-as"]
    peer_id = task.host["facts"]["Cisco-IOS-XE-bgp:neighbor"]["id"]
    print(f"{task.host} peer: {peer_id} is part of remote asn {remote_asn}")
    return Result(host=task.host, result=response.text)

result = nr.run(task=load_vars)
# print_result(result=result)
# ipdb.set_trace()
import requests

requests.packages.urllib3.disable_warnings()

device = {
    "host": "10.123.10.206",
    "port": "443",
    "user": "okan",
    "password": "hollander"
}

headers = {
    "Accept": "application/yang-data+json"
}

url = f"https://{device['host']}:{device['port']}/restconf/data/Cisco-IOS-XE-interfaces-oper:interfaces/interface=GigabitEthernet1"

response = requests.get(url=url,
                        headers=headers,
                        auth=(f"{device['user']}",f"{device['password']}"),
                        verify=False)

if response.status_code == 200:
    print(response.text)
else:
    print(response.status_code)
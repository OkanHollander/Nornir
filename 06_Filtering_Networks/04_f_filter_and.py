from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
from nornir.core.filter import F

nr = InitNornir()

def test_task(task):
    task.run(task=send_command, command="show ip int brief")


filter_task = nr.filter(F(router_type="xe") & F(company="Company1"))
result = filter_task.run(task=test_task)
print_result(result=result)

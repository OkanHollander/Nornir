from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result

nr = InitNornir()

def test_task(task):
    task.run(task=send_command, command="show ip int brief")

interface_filter = nr.filter(company="Company1", router_type="xe")
result = interface_filter.run(task=test_task)
print_result(result=result)

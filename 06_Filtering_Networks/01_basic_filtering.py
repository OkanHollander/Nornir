from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result

nr = InitNornir()

def push_banner(task):
    task.run(task=send_command, command="show ip int brief")


# Filter on 'data' section in hosts.yaml
north_targets = nr.filter(router_type="xe")

# run the filter object, that in turn passes on the mail function to run
result = north_targets.run(task=push_banner)
print_result(result=result)

from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_scrapli.functions import print_structured_result
from nornir_utils.plugins.functions import print_result
from rich import print as rprint

nr = InitNornir()

def test_task(task):
    output = task.run(task=send_command, command="show version")
    # task.host["facts"] = output.scrapli_response.genie_parse_output()
    # structured_output = task.host["facts"]
    # rprint(structured_output)

result = nr.run(task=test_task)
print_structured_result(result=result, parser="genie")

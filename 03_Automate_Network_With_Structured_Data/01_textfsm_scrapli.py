from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result

nr = InitNornir()

def test_task(task):
    clock_result = task.run(task= send_command, command="show clock")
    structured_output = clock_result.scrapli_response.textfsm_parse_output()
    print(structured_output)

result = nr.run(task=test_task)
# print_result(result=result)
from nornir import InitNornir
from nornir_netmiko.tasks import netmiko_send_command
from nornir_utils.plugins.functions import print_result

nr = InitNornir()

def test_task(task):
    clock_result = task.run(task= netmiko_send_command, command_string="show clock", use_textfsm=True)
    structured_output = clock_result.result
    print(structured_output)
    

result = nr.run(task=test_task)
# print_result(result=result)
from nornir import InitNornir
from nornir_scrapli.tasks import netconf_get_config
from nornir_utils.plugins.functions import print_result
import xmltodict
from rich import print as rprint
import ipdb

nr = InitNornir()

def pull_netconf(task):
    result = task.run(task=netconf_get_config, source="running", filter_type="xpath", filter_="/native/router")
    task.host["facts"] = xmltodict.parse(result.result)
    
    facts = task.host["facts"]
    rid = facts["rpc-reply"]["data"]["native"]["router"]["bgp"]["bgp"]["router-id"]["ip-id"]
    print(f"{task.host} had rid: {rid}")

result = nr.run(task=pull_netconf)
# print_result(result=result)
# ipdb.set_trace()
from nornir import InitNornir
from nornir_http.tasks import http_method
from nornir_utils.plugins.functions import print_result
import json
from rich import print as rprint

nr = InitNornir()
headers = {
    "Accept": "application/yang-data+json"
}


def http_task(task):
    response = task.run(task=http_method, 
                        method="get", 
                        verify=False, 
                        auth=(f"{task.host.username}", f"{task.host.password}"), 
                        headers=headers,
                        url=f"https://{task.host.hostname}:443/restconf/data/native/router")
    task.host["facts"] = json.loads(response.result)
    rprint(task.host["facts"])
    
    
result = nr.run(task=http_task)
# print_result(result=result)
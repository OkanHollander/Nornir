from nornir import InitNornir
from nornir_netmiko.tasks import netmiko_send_command
from nornir_utils.plugins.functions import print_result
from rich import print as rprint

nr = InitNornir()

def test_task(task):
    output = task.run(task=netmiko_send_command, command_string="show version", use_genie=True)
    task.host["facts"] = output.result
    rprint(task.host["facts"])
    

result = nr.run(task=test_task)
# print_result(result=result)

import yaml

# for correct indentation
class MyDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)

# open the original yaml file
with open("hosts.yaml") as f:
    yaml_file = yaml.safe_load(f)
    
    # loop through all keys (note... example)
    for y in yaml_file:
        yaml_file[y]["data"]["extra"] = "lol"
    
    # print to console
    print(yaml.dump(yaml_file, default_flow_style=False, sort_keys=False, Dumper=MyDumper))
    
# write to new file
with open("hosts2.yaml", "w") as f:
    yaml.dump(yaml_file, f, default_flow_style=False, sort_keys=False, Dumper=MyDumper)
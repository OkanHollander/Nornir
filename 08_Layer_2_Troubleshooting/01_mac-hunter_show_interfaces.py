from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
from rich import print as rprint
import os

# Clear the screen upon running the script
CLEAR = "clear"
os.system(CLEAR)

nr = InitNornir()
target = input("Enter the mac address you want to find\nExample: 1234.ABCD.5678\n")
target_list = []
print("*"*105)

def pull_info(task):
    interface_result = task.run(task=send_command, command="show interfaces")
    task.host["facts"] = interface_result.scrapli_response.genie_parse_output()
    interfaces = task.host["facts"]
    for interface in interfaces:
        try:
            mac_address = interfaces[interface]["mac_address"]
            if target.strip() == mac_address:
                target_list.append(mac_address)
                intf = interface
                print_info(task, intf)
        except KeyError:
            pass
            

def print_info(task, interface):
    rprint("[green]*** Target Identified ***[/green]")
    print(f"MAC ADDRESS: {target} is present on {task.host}'s {interface}")
    rprint("[cyan]*** Generating details ***[/cyan]")
    cdp_output = task.run(task=send_command, command="show cdp neighbor")
    task.host["cdp_facts"] = cdp_output.scrapli_response.genie_parse_output()
    index = task.host["cdp_facts"]["cdp"]["index"]
    for i in index:
        local_interface = index[i]["local_interface"]
        remote_device = ""
        remote_port = ""
        if local_interface == interface:
            remote_device = index[i]["device_id"]
            remote_port = index[i]["port_id"]
            print(f"{task.host} {local_interface} is connected to {remote_device}'s {remote_port}")
    version_result = task.run(task=send_command, command="show version")
    task.host["version_facts"] = version_result.scrapli_response.genie_parse_output()
    version = task.host["version_facts"]["version"]
    version_platform = version["platform"]
    version_version = version["version"]
    version_sn = version["chassis_sn"]
    version_uptime = version["uptime"]
    print(f"management ip: {task.host.hostname}\
        \nuptime: {version_uptime}\
            \nserial number: {version_sn}\
                \nplatform: {version_platform}\
                    \nversion: {version_version}")
    
    
result = nr.run(name="Pull Interface Info", task=pull_info)
if target not in target_list:
    
    rprint("[red]Target not found[/red]")
# print_result(result=result)

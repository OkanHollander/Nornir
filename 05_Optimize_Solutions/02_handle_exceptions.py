from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
from rich import print as rprint
import ipdb

nr = InitNornir()

def get_routing_info(task):
    output = task.run(task=send_command, command="show ip route")
    task.host["facts"] = output.scrapli_response.genie_parse_output()
    
    routes = task.host['facts']['vrf']['default']['address_family']['ipv4']['routes']
    for hop in routes:
        try:
            next_hop_list = routes[hop]['next_hop']['next_hop_list']
            rprint(f"{task.host} NEXT HOP INFO: {next_hop_list}")
        except Exception as e:
            pass

result = nr.run(task=get_routing_info)
print_result(result=result)
# ipdb.set_trace()

from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
from tqdm import tqdm

nr = InitNornir()

def test_task(task, progress_bar):
    task.run(task=send_command, command="show ip int brief")
    
    # update the progress bar after every host iteration
    progress_bar.update()


with tqdm(total=len(nr.inventory)) as progress_bar:
    result = nr.run(task=test_task, progress_bar=progress_bar)
print_result(result=result)
from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
import os
from rich import print as rprint
import ipdb
from collections import Counter
import threading


LOCK = threading.Lock()
CLEAR = "clear"
os.system(CLEAR)

nr = InitNornir()

raw_ip_list = []

def get_ip(task):
    response = task.run(task=send_command, command="show interfaces")
    task.host["facts"] = response.scrapli_response.genie_parse_output()
    interfaces = task.host["facts"]
    for interface in interfaces:
        try:
            ip_address = interfaces[interface]["ipv4"]
            for ip in ip_address:
                raw_ip_list.append(ip_address[ip]["ip"])
        except KeyError:
            pass


def locate_ip(task):
    response = task.run(task=send_command, command="show interfaces")
    task.host["facts"] = response.scrapli_response.genie_parse_output()
    interfaces = task.host["facts"]
    for interface in interfaces:
        try:
            ip_address = interfaces[interface]["ipv4"]
            for ip in ip_address:
                ipaddr = ip_address[ip]["ip"]
                if ipaddr in targets:
                    version_result = task.run(task=send_command, command="show version")
                    task.host["version_facts"] = version_result.scrapli_response.textfsm_parse_output()
                    serial = task.host["version_facts"][0]["serial"]
                    LOCK.acquire()
                    rprint(f"{task.host} {interface} - {ipaddr}")
                    rprint(f"SERIAL NUMBER: {serial}")
                    rprint(f"MNGT IP: {task.host.hostname}")
                    data = task.host.data
                    for k,v in data.items():
                        if "facts" not in k:
                            print(f"{k}: {v}")
                    print("\n")
                    LOCK.release()
        except KeyError:
            pass


result = nr.run(task=get_ip)

# new_list = Counter(raw_ip_list)
# for key, value in new_list.items():
#     if value > 1:
#         print(key)
targets = [key for key, value in Counter(raw_ip_list).items() if value > 1]
if targets:
    nr.run(task=locate_ip)
else:
    print("No duplicates found!")


# print_result(result=result)
# ipdb.set_trace()
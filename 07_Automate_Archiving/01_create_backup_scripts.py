from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
from nornir_utils.plugins.tasks.files import write_file

nr = InitNornir()

def backup_configuration(task):
    cmds = ['show run', "show version"]
    for cmd in cmds:
        # Get the results
        r = task.run(task=send_command, command=cmd)
        
        # Write results to file
        task.run(task=write_file, filename=f"Back-up/{task.host}-{cmd}.txt" , content=r.result)

result = nr.run(name="Creating Backup Archive", task=backup_configuration)
print_result(result=result)
 
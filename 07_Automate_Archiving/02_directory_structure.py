from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
from nornir_utils.plugins.tasks.files import write_file
from datetime import date
import pathlib

nr = InitNornir()

def backup_configuration(task):
    cmds = ['show run', "show version", "show ip int brief"]
    for cmd in cmds:
        # Create directory structure
        base_dir = "Back-up"
        config_dir = base_dir + "/" + "Config_Archive"
        date_dir = config_dir + "/" + str(date.today())
        command_dir = date_dir + "/" + cmd.replace(" ", "_")
        # Create the directory
        pathlib.Path(config_dir).mkdir(exist_ok=True)
        pathlib.Path(date_dir).mkdir(exist_ok=True)
        pathlib.Path(command_dir).mkdir(exist_ok=True)
        
        
        
        # Get the results
        r = task.run(task=send_command, command=cmd)
        
        # Write results to file
        task.run(task=write_file,
                 filename=""+ str(command_dir) + f"/{task.host}.txt",
                 content=r.result)

result = nr.run(name="Creating Backup Archive", task=backup_configuration)
print_result(result=result)
 
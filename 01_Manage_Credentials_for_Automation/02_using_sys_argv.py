from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
import sys

nr = InitNornir(config_file="config.yaml")
passwd = sys.argv[1] if len(sys.argv) >= 3 else ''

nr.inventory.defaults.password = passwd

def test_func(task):
    task.run(task=send_command, command="show ip int brief")


result = nr.run(task=test_func)
print_result(result=result)

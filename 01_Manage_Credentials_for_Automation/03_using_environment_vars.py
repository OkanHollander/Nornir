from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
import os

nr = InitNornir(config_file="config.yaml")
passwd = os.environ['NORNIR_PASS']

nr.inventory.defaults.password = passwd

def test_func(task):
    task.run(task=send_command, command="show ip int brief")


result = nr.run(task=test_func)
print_result(result=result)

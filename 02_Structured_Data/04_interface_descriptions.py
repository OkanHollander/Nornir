from nornir import InitNornir
from nornir_scrapli.tasks import send_command, send_configs
from nornir_utils.plugins.functions import print_result
import ipdb
from rich import print as rprint

nr = InitNornir()

def test_task(task):
    output = task.run(task=send_command, command="show cdp neighbor")
    task.host["facts"] = output.scrapli_response.genie_parse_output()
    cdp_index = task.host["facts"]["cdp"]["index"]
    for num in cdp_index:
        local_interface = cdp_index[num]["local_interface"]
        remote_port = cdp_index[num]["port_id"]
        remote_device = cdp_index[num]["device_id"]
        
        config_commands = [f"interface {local_interface}", f"description Is connected to {remote_device}'s interface: {remote_port}"]
        task.run(task=send_configs, configs=config_commands)
    
result = nr.run(task=test_task)
print_result(result=result)

    
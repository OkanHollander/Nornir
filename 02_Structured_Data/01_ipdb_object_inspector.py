from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
import ipdb
from rich import print as rprint

nr = InitNornir()

def test_task(task):
    output = task.run(task= send_command, command="show version")
    task.host["facts"] = output.scrapli_response.genie_parse_output()
    version_number = task.host["facts"]["version"]["xe_version"]
    if version_number == "17.03.02":
        rprint(f"{task.host}: [green]VERSION CHECK PASSED[/green]")
    else:
        rprint(f"{task.host}: [red]VERSION CHECK FAILED[/red]")
    
result = nr.run(task=test_task)
# print_result(result=result)
# ipdb.set_trace()

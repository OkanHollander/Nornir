from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
import ipdb
from rich import print as rprint

nr = InitNornir()

def test_task(task):
    output = task.run(task=send_command, command="show interfaces")
    task.host["facts"] = output.scrapli_response.genie_parse_output()
    multicast_groups = task.host["facts"]["GigabitEthernet1"]["multicast_groups"]
    for multi in multicast_groups:
        print(multi)

result = nr.run(task=test_task)
# print_result(result=result)
ipdb.set_trace()
    
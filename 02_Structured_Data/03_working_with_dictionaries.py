from nornir import InitNornir
from nornir_scrapli.tasks import send_command
from nornir_utils.plugins.functions import print_result
import ipdb
from rich import print as rprint

nr = InitNornir()

def test_task(task):
    output = task.run(task=send_command, command="show ip bgp summary")
    task.host["facts"] = output.scrapli_response.genie_parse_output()
    neighbors = task.host["facts"]["vrf"]["default"]["neighbor"]
    for key, value in neighbors.items():
        updown = neighbors[key]["address_family"][""]["up_down"]
        rprint(f"{task.host} neighbor {key} updown value is {updown}")

result = nr.run(task=test_task)
# print_result(result=result)
# ipdb.set_trace()
    